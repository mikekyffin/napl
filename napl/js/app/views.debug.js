﻿// file:            views.debug.js
// author(s):       mike kyffin
// build target:    view.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

define([
    // esri or dojo dependencies
].concat([
    // our dependencies (local or deployed) 
    "app/" + (document.location.host.match(/^localhost/i) ? "widgets.debug" : "widgets")
]), function (widgets) {
    var views = {};
    // the view!
    views.MetadataView = function (_data) {
        this.data = _data;
        this.domNode = $("<div>").addClass("metadata-view");
        return this;
    };

    views.MetadataView.prototype.validating = function () {
        this.setIcon(this.mainIcon, "spin", true);
    };

    views.MetadataView.prototype.setValid = function (addCallback, dlCallback) {
        this.viewText.click(function () {
            addCallback();
        });
        this.downloadText.click(function () {
            dlCallback();
        });
        this.setIcon(this.mainIcon, "check", true);
        this.setIcon(this.viewIcon, "map-marker", true);
        this.setIcon(this.downloadIcon, "download", true);
        this.setDownloadText("Download", true);
        $(".is-georeferenced", this.domNode).css("display", "block");
    };

    views.MetadataView.prototype.endValidation = function () {
        this.setIcon(this.mainIcon, "check", true);
    }
    /*
        @icon: an <i> font awesome element
        @type: text of icon type (check, arrow, whatever...see: http://fortawesome.github.io/Font-Awesome/icons/)
        @isVisible: boolean for display = none or inline-block
    */
    views.MetadataView.prototype.setIcon = function (icon, type, isVisible) {
        icon.removeClass();
        if (type == "spin") {
            icon.addClass("metadata-view-icon-elem fa fa-refresh fa-" + type);
        } else {
            icon.addClass("metadata-view-icon-elem fa fa-" + type);
        }
        icon.css({
            display: (isVisible) ? "inline-block" : "none"
        });
    }
   
    views.MetadataView.prototype.setDownloadText = function (text, isLink) {
        this.downloadText.html(text);
        if (isLink) {
            this.downloadText.addClass("is-valid");
        }
    }
    // this is the long one. we're explicitly calling fields because we want the
    // UI to be something other than just iterating the key/value pairs into a list.
    // i know it's hideous.
    views.MetadataView.prototype._render = function () {
        var _list = $("<table>");

        var _status = $("<span>", {
            html: this.data["STATUS"]          
        });

        this.mainIcon = $("<i>", {});

        var _top = $("<div>")
            .append(_status)
            .append(this.mainIcon);

        this.viewIcon = $("<i>", {style: "width:12px;text-align:center;"});
        this.viewText = $("<span>", {
            "html": "View",
            "class": "is-valid"
        });

        var _mid = $("<div>", {
            "style": "display:none;",
            "class": "is-georeferenced"
        })
            .append(this.viewIcon)
            .append(this.viewText);

        this.downloadText = $("<span>", {
            "class": "is-valid"
        });

        this.downloadIcon = $("<i>", {style: "width:12px;text-align:center;"});

        var _bot = $("<div>",{
            "style": "display:none;",
            "class": "is-georeferenced"
        })
            .append(this.downloadIcon)
            .append(this.downloadText);
            

        var cell = $("<td>")
            .append(_top)
            .append(_mid)
            .append(_bot);
            

        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "Photo"
            }))
            .append($("<td>", {
                "html": this.data["ROLL"] + "-" + this.data["PHOTO"] + ", (Line No. " + this.data["LINE_NO"] + ")"
            }));
        /* */
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", { "html": "Status" }))
            //.append($("<td>")
            //    .append(this._link)
            //    .append(this._loader)
            //    .append(this._check)
        //);
            .append(cell);
        /* */
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "Date"
            }))
            .append($("<td>", {
                "html": this.data["NAPL_DATE"]
            }));
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "Box"
            }))
            .append($("<td>", {
                "html": this.data["BOX"]
            }));
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "NTS Map"
            }))
            .append($("<td>", {
                "html": this.data["NTS_MAP"]
            }));
        $("<tr>")
         .appendTo(_list)
         .append($("<td>", {
             "html": "Area"
         }))
         .append($("<td>", {
             "html": this.data["AREA"]
         }));
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "Envelope"
            }))
            .append($("<td>", {
                "html": "[" + this.data["Y_NO"] + ", " + this.data["X_NO"] + "]" + "[" + this.data["Y_NE"] + ", " + this.data["X_NE"] + "]" + "[" + this.data["Y_SE"] + ", " + this.data["X_SE"] + "]" + "[" + this.data["Y_SO"] + ", " + this.data["X_SO"] + "]"
            }));
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "Centre"
            }))
            .append($("<td>", {
                "html": "[" + this.data["CNTR_LAT"] + ", " + this.data["CNTR_LON"] + "]"
            }));
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "Size"
            }))
            .append($("<td>", {
                "html": "Height: " + this.data["HEIGHT"] + ", Width: " + this.data["WIDTH"]
            }));
        $("<tr>")
           .appendTo(_list)
           .append($("<td>", {
               "html": "Angle"
           }))
           .append($("<td>", {
               "html": this.data["ANGLE"]
           }));
        $("<tr>")
           .appendTo(_list)
           .append($("<td>", {
               "html": "Season"
           }))
           .append($("<td>", {
               "html": this.data["SEASON"]
           }));
        $("<tr>")
           .appendTo(_list)
           .append($("<td>", {
               "html": "Altitude"
           }))
           .append($("<td>", {
               "html": this.data["ALTITUDE"]
           }));
        $("<tr>")
          .appendTo(_list)
          .append($("<td>", {
              "html": "Scale"
          }))
          .append($("<td>", {
              "html": this.data["SCALE"]
          }));
        $("<tr>")
          .appendTo(_list)
          .append($("<td>", {
              "html": "Camera"
          }))
          .append($("<td>", {
              "html": this.data["CAMERA"] + ", Focal Length: " + this.data["FOCAL_LEN"]
          }));
        $("<tr>")
          .appendTo(_list)
          .append($("<td>", {
              "html": "Frames"
          }))
          .append($("<td>", {
              "html": this.data["FRAMES"]
          }));
        $("<tr>")
          .appendTo(_list)
          .append($("<td>", {
              "html": "Film"
          }))
          .append($("<td>", {
              "html": "Colour: " + this.data["FILM_COLOR"] + ", Size: " + this.data["FILM_SIZE"]
          }));
        $("<tr>")
            .appendTo(_list)
            .append($("<td>", {
                "html": "Lens"
            }))
            .append($("<td>", {
                "html": "Name: " + this.data["LENS_NAME"] + ", Flinch: " + this.data["LENSFLINCH"] + ", Filter: " + this.data["FILTER"]
            }));
        // return it all, baby!
        return _list;
    };
    //
    views.MetadataView.prototype.render = function (_target) {
        this.domNode.append(this._render());
        _target.append(this.domNode);      
        $(this).trigger("create", this);  // fire 'create' event for listeners
    };
    //   
    views.MetadataView.prototype.show = function () {
        this.domNode.removeClass("metadata-view-hide");
        this.domNode.addClass("metadata-view-show");
        $(this).trigger("show-envelope");
    };
    //
    views.MetadataView.prototype.hide = function () {
        this.domNode.removeClass("metadata-view-show");
        this.domNode.addClass("metadata-view-hide");
        $(this).trigger("hide-envelope");
    };
    // return the views object - we'll need it.
    return views;
});