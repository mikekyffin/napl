﻿// file:            ui.debug.js
// author(s):       mike kyffin
// build target:    ui.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

define([
     // esri or dojo dependencies
].concat([
    // our dependencies (local or deployed) 
    "app/" + (document.location.host.match(/^localhost/i) ? "identify.debug" : "identify"),
    "app/" + (document.location.host.match(/^localhost/i) ? "widgets.debug" : "widgets"),
    "app/" + (document.location.host.match(/^localhost/i) ? "collection.debug" : "collection"),
    "app/" + (document.location.host.match(/^localhost/i) ? "views.debug" : "views"),
    "app/" + (document.location.host.match(/^localhost/i) ? "ga.debug" : "ga"),
    "app/" + (document.location.host.match(/^localhost/i) ? "map.debug" : "map"),
    "app/" + (document.location.host.match(/^localhost/i) ? "layer.debug" : "layer")
]), function (identify, widgets, collection, views, ga, map, layer) {
    // closured stuff
    var _outlineColour = new dojo.Color([0, 0, 0]);
    var _fillColour = new dojo.Color([255, 255, 0, 0.25]);
    var _lineSymbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, _outlineColour, 2);
    var _fillSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, _lineSymbol, _fillColour);
    return {
        // this will hold the whole map object
        Map: null,
        // init the ui and call anythign to be built with js
        // TODO: why do I need Map here?
        init: function (_config, _map) {
            this._config = _config;
            this.Map = _map.Map
            this._buildSettings();
            this._buildHelp();
            this._trackLinks();
            // set-up the click handlers for all the pane opening links/icons
            $(".pane").click(function () {
                var target = $(this).data("pane-target");
                // show the dialog panel
                $("#dialog-wrapper").css("display", "block");
                // hide all the subdialog contents
                $(".dialog-subdialog").css("display", "none");
                // show only the proper subdialog content
                $("div[data-target='" + target + "']").css("display", "block");
            });

            $.each($(".dialog-subdialog"), function (i, o) {
                var _close = new widgets.Icon("fa-times-circle fa-2x")
                    .addClass("widget widget-icon widget-icon-action")
                    .click(function () {
                        $("#dialog-wrapper").css("display", "none");
                    })
                    .appendTo($(".dialog-subdialog-buttons", this));
            });
        },
        //
        _trackLinks: function () {
            $(".links-item a").click(function () {
                ga.track({
                    "eventCategory": "Link",
                    "eventAction": "click",
                    "eventLabel": $(this).attr("href"),
                    "eventValue": null
                })
            });
        },
        // empty the content of the result pane as necessary
        onResultStart: function () {
            $("#dialog-wrapper").css("display", "block");
            $("#results .dialog-subdialog-content").empty();
            $("#results .dialog-subdialog-title").empty();
            $("#results .widget-icon-removeable").remove();
            $(".dialog-subdialog").css("display", "none");
            $("#" + "results").css("display", "block");
        },
        // display the "updating..." message and spinner or provide.
        // some other message message.
        // TODO: write something that shows the "loader" with the image - so it will just be a message pane that fades 
        loaderOn: function (msg) {
            $(".loader").css("display", "block");
            $(".loader .loader-message").html((msg) ? msg : "Updating");
        },
        // hide the loader
        loaderOff: function () {
            $(".loader").css("display", "none");
        },
        // build any UI stuff neded for the 'results pane'
        _buildResults: function () {
            var _close = new widgets.Icon("fa-times-circle fa-2x")
                .addClass("widget widget-icon widget-icon-action")
                .click($.proxy(function () {
                    $("#dialog-wrapper").css("display", "none");
                }, this))
                .appendTo($("#results .dialog-subdialog-buttons"));
        },
        // go get the legend from REST
        _renderLegend: function (_target) {
            var _wrap = $("<div>")
                .addClass("legend")
                .appendTo(_target);

            var _title = $("<div>", {
                "html": "Legend",
                "class": "legend-title"
            }).appendTo(_wrap);

            var _load = $("<div>")
                .addClass("legend-loader-item")
                .append($("<span>", {
                    "html": "...loading legend"
                }))
                .append("<i class=\"fa fa-refresh fa-spin\"></i>")
                //.append($("<img>", {
                //    "src": "css/images/loader.gif",
                //    "style": "vertical-align: middle"
                //}))
                .appendTo(_wrap);
            // ajax call to actually get the legend.
            // by doing it this way we don't have to worry about making edits to the symbology
            // of the map behind the service - we can just change what we want and it'll be instantly live.
            var xhr = $.getJSON(this._config.PROXY_URL + "?" + this._config.SERVICE_URL + "/legend" + "?" + $.param({
                "f": "json"
            }));
            xhr.done($.proxy(function (data) {
                if (data) {
                    $.each(data.layers, $.proxy(function (i, layer) {
                        var _title = $("<div>", {
                            "html": layer["layerName"]
                        }).appendTo(_wrap);
                        var _list = $("<ul>").appendTo(_title);
                        $.each(layer.legend, $.proxy(function (n, item) {
                            var li = $("<li>").appendTo(_list);
                            $("<img>", {
                                "src": this._config.PROXY_URL + "?" + this._config.SERVICE_URL + "/" + i + "/images/" + item.url
                            }).appendTo(li);
                            $("<span>", {
                                "html": item.label
                            }).appendTo(li);
                        }, this));
                    }, this));
                }
            }, this));
            xhr.fail(function () {
                _wrap.append($("<span>", {
                    "html": "Error: unable to communicate with legend specification."
                }).addClass("error"));
            });
            xhr.always(function () {
                $(".legend-loader-item").css("display", "none");
            });
        },
        // build what's needed in the options (tool) pane
        _buildSettings: function () {
            // go get the legend and render it
            this._renderLegend($("#settings .dialog-subdialog-content"));
            // make me an opacity controller                
            this._renderOKnob($("#settings .dialog-subdialog-content"));
        },

        HtmlContent: function (html, title, isVisible) {
            // parent container
            this._domNode = $("<div>", {
                "class": "help-margin-top-bottom"
            });

            var _title = $("<span>", {
                "html": title,
                "class": "help-title"
            })
                .click(function () {
                    if (_content.css("display") == "block") {
                        _show.css("display", "inline-block");
                        _hide.css("display", "none");
                        _content.css("display", "none");
                    } else {
                        _show.css("display", "none");
                        _hide.css("display", "inline-block");
                        _content.css("display", "block");
                    }

                })
                .appendTo(this._domNode);

            var _content = $("<div>", {
                "class": "help-margin-right-left"
            })
                .css({
                    "display": (isVisible) ? "none" : "display-block"
                })
               .load(html)
               .appendTo(this._domNode);

            var _show = $("<span>", {
                "class": "help-plus-minus"
            })
                .html("<i class=\"fa fa-plus\"></i>")
                .css({
                    "display": (isVisible) ? "none" : "display-block"

                })
                .appendTo(_title);

            var _hide = $("<span>", {
                "class": "help-plus-minus"
            })
                .html("<i class=\"fa fa-minus\"></i>")
                .css({
                    "display": (isVisible) ? "display-block" : "none"
                })
                .appendTo(_title);

            return this._domNode;
        },
        //
        _buildHelp: function () {
            $("#aboutContent").html(new this.HtmlContent("about.html", "About", true));
            $("#helpContent").html(new this.HtmlContent("help.html", "Help", false));
        },
        //
        _renderOKnob: function (_target) {

            // something to hold the knob
            var _wrap = $("<div>")
                .addClass("knob-wrap")
                .appendTo(_target);

            // label
            $("<div>", {
                "html": "Layer Opacity (%)"
            })
                .addClass("knob-label")
                .appendTo(_wrap);


            // i doubt i'm done with this - the ui style/config is horrdendous
            // consider switching to a slider. really.
            var _knob = $("<input>", {
                "value": 75,
                "min": 0,
                "max": 100,
                "thickness": 0.3,
                "displayInput": true,
                "angleOffset": 0
            }).knob({
                "width": 100,
                "height": 100,
                "bgColor": "#F2F5F7",
                "fgColor": "#5F89AA",
                "change": $.proxy(function (v) {
                    this.Map.getLayer("napl").setOpacity(v / 100);
                }, this)
            });
            _knob.appendTo(_wrap);

        },
        // is asynchronous
        _validatePhoto: function (_url, _photoName) {
            return layer.validatePhoto(_url, _photoName);
        },
        //
        _getAirPhotoServiceUrl: function (/*String*/ year) {
            return this._config.SERVICE_ROOT + "/arcgis/rest/services/airphoto/y" + year + "/MapServer";
        },
        //
        _getAirPhotoName: function (data) {
            return data["ROLL"] + "-" + ("000".substring(0, "000".length - data["PHOTO"].length) + data["PHOTO"]) + ".tif";
        },
        //
        _extendResult: function (_result) {
            // build a polygon, fill a ring and pack it into an esri.Graphic
            var polygon = new esri.geometry.Polygon(new esri.SpatialReference({ wkid: 4326 }));
            polygon.addRing([
                [parseFloat(_result.attributes["X_NO"]), parseFloat(_result.attributes["Y_NO"])],
                [parseFloat(_result.attributes["X_NE"]), parseFloat(_result.attributes["Y_NE"])],
                [parseFloat(_result.attributes["X_SE"]), parseFloat(_result.attributes["Y_SE"])],
                [parseFloat(_result.attributes["X_SO"]), parseFloat(_result.attributes["Y_SO"])],
                [parseFloat(_result.attributes["X_NO"]), parseFloat(_result.attributes["Y_NO"])]
            ]);
            var graphic = new esri.Graphic(new esri.geometry.geographicToWebMercator(polygon), _fillSymbol, null, null);
            return $.extend(_result.attributes, {
                "geometry": _result.geometry,
                "geometryType": _result.geometryType,
                "graphic": graphic
            });
        },

        _addPhotoLayer: function (serviceYear, photo) {
            var photoLayerUrl = this._config.SERVICE_ROOT + "/arcgis/rest/services/airphoto/" + serviceYear + "/MapServer";
            var photoLayer = new layer.Layer(photoLayerUrl, {});
            var hook = dojo.connect(photoLayer, "onLoad", this, function () {
                photoLayer.setPhotos([photo]);
                dojo.disconnect(hook);
            });
            map.Map.addLayer(photoLayer);
        },

        _onDownloadComplete: function (view, result) {
            var link = $("<a>", {
                href: this._config.DOWNLOAD_AUTH + this._config.DOWNLOAD_DISCLAIMER + "?" + $.param({
                    "proj": "airphoto",
                    "year": view["data"]["YEAR"], //test! 
                    "file": result["value"]["url"]
                }),
                html: "Click to download",
                target: "_blank"
            });
            $(view.downloadText).off("click");
            view.setIcon(view.downloadIcon, "check", true);
            view.setDownloadText(link, true);
        },

        _onDownloadSubmitError: function (view) {
            view.downloadText.off("click");
            view.setIcon(view.downloadIcon, "exclamation", true);
            view.setDownloadText("Error submitting the job", false);
        },

        _onDownloadStatusError: function (view) {
            view.downloadText.off("click");
            view.setIcon(view.downloadIcon, "exclamation", true);
            view.setDownloadText("Communication error with download task", false);
        },

        _onDownloadStatusUpdate: function(view, message) {
            view.downloadText.off("click");
            view.setIcon(view.downloadIcon, "spin", true);          
            view.setDownloadText(message, false);
        },

        _onDownloadOutputError: function (view) {
            view.downloadText.off("click");
            view.setIcon(view.downloadIcon, "exclamation", true);
            view.setDownloadText("Error getting output file", false);
        },

        _downloadPhoto: function (serviceYear, photo, view) {
            var downloadTool = this._config.SERVICE_ROOT + "/arcgis/rest/services/tools/GetTile/GPServer/GetTile";
            var downloadParams = {
                f: "json",
                Project: "airphoto/" + serviceYear,
                Tile: photo
            };
            view.setIcon(view.downloadIcon, "spin", true);
            var gp = new esri.tasks.Geoprocessor(downloadTool);
            gp.submitJob(
                downloadParams,
                function (response) { // success                    
                    if (response.jobStatus === "esriJobSucceeded") {
                        gp.getResultData(
                            response.jobId,
                            "Output",
                            function (out) {
                                this._onDownloadComplete(view, out);
                            }.bind(this),
                            function (err) {
                                this._onDownloadOutputError(view);
                            }.bind(this));
                    } else { // fail?
                        this._onDownloadOutputError(view);
                    }
                }.bind(this),
                function (jobInfo) { // update message to client
                    this._onDownloadStatusUpdate(view, (jobInfo.messages.length > 0) ? jobInfo.messages[jobInfo.messages.length - 1].description : "");
                }.bind(this),
                function (e) { // submission error
                    this._onDownloadSubmitError(view);
                }.bind(this));
        },
        //
        _onValidationSuccess: function (resp, view) {
            if (resp.results && resp.results.length > 0) {
                // instruct ui to let the photo be available as a click-able resource
                function _add() {
                    this._addPhotoLayer.call(this, resp.results[0].layerName, resp.results[0].value);
                }
                function _download() {
                    this._downloadPhoto.call(this, resp.results[0].layerName, resp.results[0].value.replace(/\..*/, ""), view);
                }
                view.setValid(_add.bind(this), _download.bind(this));
            } else {
                view.setIcon(view.mainIcon, "check", false);
            }
        },
        //
        _onGeoCreate: function (evt, view) {
            var serviceUrl = this._getAirPhotoServiceUrl(view.data["NAPL_DATE"].substring(view.data["NAPL_DATE"].length - 4));
            var photoName = this._getAirPhotoName(view.data);
            view.validating();
            var validate = layer.validatePhoto(this._config.PROXY_URL + "?" + serviceUrl, photoName);
            validate.done(function (resp) {
                this._onValidationSuccess(resp, view);
            }.bind(this));
            validate.fail(function () {

            });
            validate.always(function () {

            });
        },
        _onValidationError: function () {

        },
        //
        _onIdentifySuccess: function (response) {
            if (response.results && response.results.length > 0) {
                var coll = new collection.MetadataCollection();
                $(coll).on("navigate", function () {
                    layer.clearLayers(map.Map);
                });
                var attrs = $.map(response.results, $.proxy(function (result) {
                    var attr = this._extendResult(result);
                    var view = new views.MetadataView(attr);
                    $(view).on("show-envelope", function () {
                        map.Map.graphics.add(view.data.graphic);
                    });
                    $(view).on("hide-envelope", function () {
                        map.Map.graphics.remove(view.data.graphic);
                    });
                    if (result.attributes["STATUS"] == "Georeferenced") {
                        $(view).on("create", $.proxy(function (evt, view) {
                            this._onGeoCreate(evt, view);
                        }, this));
                    }
                    // add to the collection
                    coll.add(view);
                }, this));
                // write the whole collection into the DOM
                coll.render($("#results .dialog-subdialog-content"));
            } else {
                // ags returns an empty array if it doesn't find anything. handle it.
                this._onIdentifyNoResult();
            }
        },
        // what happens when we have a successful operation, but nothing is found
        _onIdentifyNoResult: function () {
            $("#results .dialog-subdialog-content")
                .css("text-align", "center")
                .append($("<span>", {
                    "html": "<br />No air photo found at this location!"
                }).addClass("msg-no"));
        },
        // error?
        _onIdentifyError: function (err) {
            $("#results .dialog-subdialog-content")
                .css("text-align", "center")
                .append($("<span>", {
                    "html": "<br />Error: unable to communicate with GIS service!"
                }).addClass("msg-no"));
        },
        //
        _onIdentifyAlways: function () {
            this.loaderOff();
        },
        // get me some air photos! (maybe?)
        identify: function (evt) {
            layer.clearLayers(map.Map);
            this.loaderOn("Searching for metadata...");
            map.Map.graphics.clear();
            var ll = esri.geometry.xyToLngLat(evt.mapPoint.x, evt.mapPoint.y)
            ga.track({
                'eventCategory': "Identify", // required.                
                'eventAction': ll[1] + "," + ll[0], // required.      
                'eventLabel': null,
                'eventValue': null
            });
            var xhr = identify.doIdentify(evt, this.Map, this._config);
            xhr.done($.proxy(function (response) {
                this.onResultStart();
                this._onIdentifySuccess(response);
            }, this));
            xhr.fail($.proxy(function (err) {
                this._onIdentifyError(err)
            }, this));
            xhr.always($.proxy(function () {
                this._onIdentifyAlways();
            }, this));
        },
        // centre the map at the click point and go to the LOD where the flight lines become visible
        gotoLines: function (evt) {
            this.Map.centerAndZoom(evt.mapPoint, 7);
        },
        // centre the map at the click point and go to the LOD where the airphotos become visible
        gotoPoints: function (evt) {
            this.Map.centerAndZoom(evt.mapPoint, 11);
        }
    };

});
