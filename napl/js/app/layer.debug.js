﻿// file:            layer.debug.js
// author(s):       mike kyffin
// build target:    layer.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!


define([
    // esri or dojo dependencies
].concat([
    // our dependencies (local or deployed)    
]), function (widgets) {

    //
    function Layer(url, options) {
        esri.layers.ArcGISDynamicMapServiceLayer.call(this, url, options);
        this.displayedTiles = [];
        this.setVisibleLayers([0]);
    }    
    Layer.prototype = Object.create(esri.layers.ArcGISDynamicMapServiceLayer.prototype);
    Layer.prototype._role = "external";
    Layer.prototype._arrayToRange = function (arr) {
        var str = "(";
        for (var i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                str += ("'" + arr[i] + "'");
            }
            else {
                str += ("'" + arr[i] + "',");
            }
        }
        str += ")";
        return str;
    };
    Layer.prototype.setPhotos = function (array) {
        // just set the object's internal list of tiles. it's good to know.
        this.displayedTiles = array;
        // the string magic
        var range = this._arrayToRange(array);
        // note that we're implicitly pushing to index [0] because
        // that's the only layer within the servce. if we had other layers
        // and/or the tiles weren't in [0], we couldn't do this - we'd
        // have to actually supply the index.
        var layerDefinitions = [];
        layerDefinitions.push("Name IN " + range);
        this.setLayerDefinitions(layerDefinitions, false);
    };
    // async
    Layer.prototype.validatePhoto = function (url, photoName) {
        return $.getJSON(url + "/find?" + $.param({
            searchText: photoName,
            f: "json",
            layers: 0,
            returnGeometry: false
        }));
    };

    return {
        Layer: Layer,
        clearLayers: function (map) {
            $.each(map.layerIds, function (i, o) {
                var layer = map.getLayer(o);
                if (layer["_role"] && layer["_role"] != "internal") {
                    map.removeLayer(layer);
                }
            });
        },
        validatePhoto: function(serviceUrl, photoName) {
            return $.getJSON(serviceUrl + "/find?" + $.param({
                searchText: photoName,
                f: "json",
                layers: 0,
                returnGeometry: false
            }));
        },
        isInternal: function (layer) {
            return (layer["_role"] && layer["_role"] == "internal");
        }
    }
});