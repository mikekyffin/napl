﻿// file:            collection.debug.js
// author(s):       mike kyffin
// build target:    collection.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!


define([
    // esri or dojo dependencies
].concat([
    // our dependencies (local or deployed)
    "app/" + (document.location.host.match(/^localhost/i) ? "widgets.debug" : "widgets")
]), function (widgets) {
    //
    var collection = {};
    // basic "constructor"
    collection.MetadataCollection = function () {
        this.currentIdx = 0;
        this.views = [];
        return this;
    };
    //
   collection.MetadataCollection.prototype.find = function (_id) {
        return $.map(this.views, function (elem, i) {
            return (elem["ID"] == _id);
        })[0];
    };
    // 
    collection.MetadataCollection.prototype.add = function (view) {
        this.views.push(view)
    };
    // show previous
    collection.MetadataCollection.prototype.prev = function () {
        if (this.currentIdx != 0) {
            $(this).trigger("navigate");
            this._hideAll();
            this.currentIdx--;
            this.views[this.currentIdx].show();
        }
    };
    // show next
    collection.MetadataCollection.prototype.next = function () {
        if (this.currentIdx < this.views.length - 1) {
            $(this).trigger("navigate");
            this._hideAll();
            this.currentIdx++;
            this.views[this.currentIdx].show();
        }
    };
    // hide *all* the views in the collection from the ui
    collection.MetadataCollection.prototype._hideAll = function () {
        $.each(this.views, function (i, v) {
            v.hide();
        });
    };
    // prepares all the views to be written into the DOM
    collection.MetadataCollection.prototype.render = function (_target) {
        // define the location for the buttons to browse the results.
        // make sure they are removeable.
        var btns = $(".dialog-subdialog-buttons", _target.parent());
        // the "show me the next one" button
        $(".widget-icon-removeable", btns).remove();
        var _next = new widgets.Icon("fa-arrow-circle-right fa-2x")
            .addClass("widget widget-icon widget-icon-removeable widget-icon-action")
            .css("display", "inline-block")
            .css("vertical-align", "middle")
            .click($.proxy(function (evt) {
                evt.preventDefault();
                this.next();
                $(".pag-update").html(this.currentIdx + 1);
            }, this))
            .prependTo(btns);
        // the "show me the last one" button
        var _prev = new widgets.Icon("fa-arrow-circle-left fa-2x")
            .addClass("widget widget-icon widget-icon-removeable widget-icon-action")
            .css("display", "inline-block")
            .css("vertical-align", "middle")
            .click($.proxy(function (evt) {
                evt.preventDefault();
                this.prev();
                $(".pag-update").html(this.currentIdx + 1);
            }, this))
            .prependTo(btns);
        // define a container for the results title content
        var _pagnote = $("<span>", {
            "class": "pagnote"
        });
        // update the results title content
        $("<span>", {
            "html": 1
        })
            .addClass("pag-update")
            .appendTo(_pagnote);
        $("<span>", {
            "html": " of "
        })
            .appendTo(_pagnote);
        $("<span>", {
            "html": this.views.length
        })
            .appendTo(_pagnote);

        // set the results title content
        $(".dialog-subdialog-title", _target.parent()).html(_pagnote);
        // a div to hold everything neatly
        var _results = $("<div>")
            .addClass("metadata-view-collection");
        // write all the views in.
        // show only the first one.
        $.each(this.views, function (i, v) {
            v.render(_results);
            (i === 0) ? v.show() : v.hide();
        });
        // send everything to the DOM
        _target.append(_results);
    };
    return collection;
});