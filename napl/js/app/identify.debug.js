﻿// file:            identify.debug.js
// author(s):       mike kyffin
// build target:    identify.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

define([
     // esri or dojo dependencies
].concat([   
   // our dependencies (local or deployed) 
]), function () {
    return {
        // send an async request to the rest end-point of the service to get the features at point x,y
        doIdentify: function (evt, Map, _config) {
            return $.getJSON(_config.PROXY_URL + "?" + _config.SERVICE_URL + "/identify" + "?" + $.param({
                "f": "json",
                "geometryType": "esriGeometryPoint",
                "geometry": evt.mapPoint.x + "," + evt.mapPoint.y,
                "layers": "visible:0", // only query the air photos layer of the service, and only when it's visible
                "tolerance": 12,
                "mapExtent": Map.extent.xmin + "," + Map.extent.ymin + "," + Map.extent.xmax + "," + Map.extent.ymax,
                "sr": Map.wkid,
                "imageDisplay": window.screen.height + "," + window.screen.width + "," + "96",
                "returnGeometry": true
            }));
        }
    }
});