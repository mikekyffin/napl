﻿// file:            map.debug.js
// author(s):       mike kyffin
// build target:    map.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

define([
     // esri or dojo dependencies
    "esri/map",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/layers/FeatureLayer",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/renderers/SimpleRenderer",
    "dojo/_base/Color",
    "dojo/on",
    "dojo/_base/connect",
    "dojo/_base/lang"
].concat([
    // our dependencies (local or deployed)    
]), function (Map, ArcGISDynamicMapServiceLayer, FeatureLayer, SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, SimpleRenderer, Color, on, connect, lang) {
    return {
        Map: null,
        // init the map
        init: function (_config, ui) {
            this.Map = new Map("map", {
                "basemap": "topo",
                "center": [-96.584, 65.397], // long, lat
                "zoom": 3,
                "autoResize": true //,
                //"wrapAround180": false
            });
            // extend topo basemap to hold a searchable property to denote as internal
            $.extend(this.Map.getLayer(this.Map.layerIds[0]), {
                "_role": "internal"
            });

            // after the map has been created, we can then make our event connections
            connect.connect(this.Map, "onLoad", lang.hitch(this, function (_map) { // map is returned!
                var _naplLayer = new esri.layers.ArcGISDynamicMapServiceLayer(_config.SERVICE_URL, {
                    "_role": "internal", // also internal
                    "id": "napl",
                    "opacity": 0.75 // how transparent do we want this?
                });
                $.extend(_naplLayer, {
                    "_role": "internal"
                });
                // add the service 
                _map.addLayer(_naplLayer);
                // set the identify operation on each single click of the map
                connect.connect(_map, "onClick", ui, function (evt) {
                    var lod = _map.getLevel();
                    if (lod >= 10) {
                        this.identify(evt);
                    } else if (lod >= 7 && lod < 10) {
                        this.gotoPoints(evt);
                    } else if (lod <= 6) {
                        this.gotoLines(evt);
                    }
                });
                // show the "updating..." message
                connect.connect(_map, "onUpdateStart", ui, function (evt) {
                    this.loaderOn("Updating...");
                });
                // hide the "updating..." message
                connect.connect(_map, "onUpdateEnd", ui, function (err) {
                   this.loaderOff();
                });

                // add this to force Dojo to add a random variable to GP GET URLs. important for IE11.
                // http://forums.arcgis.com/threads/88625-IE-caching-responses-to-GP.submitJob-callbacks?highlight=submitjob
                esri.setRequestPreCallback(function (ioArgs) {
                    if (ioArgs.url.indexOf("GPServer") > -1) {
                        ioArgs.preventCache = true;
                    }
                    return ioArgs;
                });
            }));

            
        }
    }
});