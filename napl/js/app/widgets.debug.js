﻿// file:            widgets.debug.js
// author(s):       mike kyffin
// build target:    widgets.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

define([
    // esri or dojo dependencies
].concat([
    // our dependencies (local or deployed)
]), function () {
    // just create a span that has an image in it and nothing else. see css.
    return {
        "Icon": function (iconType) {
            return $("<i>").addClass("widget-icon fa " + iconType);                                 
        }       
    }
});