﻿// file:            app.debug.js
// author(s):       mike kyffin
// build target:    app.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

define([
    // esri or dojo dependencies
].concat([
    // our dependencies (local or deployed)
    "app/" + (document.location.host.match(/^localhost/i) ? "map.debug" : "map"),
    "app/" + (document.location.host.match(/^localhost/i) ? "ui.debug" : "ui")
]),
function (map, ui) {
    // all properties/functions here prefixed with _ are considered private
    var _config = {
        "SERVICE_ROOT": "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca"),
        "SERVICE_URL": "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/" + "NAPLIndex/NAPLIndex/MapServer",
        "PROXY_URL": "handlers/proxy.ashx",
        "DOWNLOAD_RASTER_URL": "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/tools/GetTile/GPServer/GetTile",      
        "DOWNLOAD_AUTH": "http://web2.trentu.ca:2048/login?url=",  // download authenitcation for zip file        
        "DOWNLOAD_DISCLAIMER": "http://lonestar.trentu.ca/ORTHOS/login/ap_disclaimer.asp" // download license disclaimer, etc.
    };
    return {
        // set esri configurations and init modules as mecessary
        init: function () {
            // this tells the js api to always call on our proxy to be prepended to internal ajax calls (and such)
            esri.config.defaults.io.proxyUrl = _config.PROXY_URL;
            esri.config.defaults.io.alwaysUseProxy = true;
            // start up the map and build the ui
            map.init(_config, ui);
            ui.init(_config, map);
        }
    }
});